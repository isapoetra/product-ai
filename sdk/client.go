/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
)

const ResponseTypeJson = "json"

type Client struct {
	AccessKey string
	SecretKey string
	Scheme    string
	Host      string
	Version   string
	Verbose   bool
}

func (client *Client) getUri(reqData IDataRequest) string {
	return client.Scheme + "://" + client.Host + "/" + reqData.GetEndPoint()
}

func (client *Client) GetResponse(reqData IDataRequest, params interface{}, result IDataResponse) (err error) {
	switch reqData.GetResponseType() {
	case ResponseTypeJson:
		return client.getJSONResponse(reqData, params, result)
	}
	return errors.New("Unhandled " + reqData.GetResponseType())
}

//noinspection GoDeferInLoop
func (client *Client) postForm(reqData IDataRequest, values map[string]io.Reader, result IDataResponse) (err error) {
	uri := client.getUri(reqData)
	log.Println(reqData.GetMethod() + ":" + uri)
	var buffer bytes.Buffer
	w := multipart.NewWriter(&buffer)
	for key, r := range values {
		var fw io.Writer
		if x, ok := r.(io.Closer); ok {
			defer func() { _ = x.Close() }()
		}
		// Add an image file
		if x, ok := r.(*os.File); ok {
			if fw, err = w.CreateFormFile(key, x.Name()); err != nil {
				return
			}
		} else {
			// Add other fields
			if fw, err = w.CreateFormField(key); err != nil {
				return
			}
		}
		if _, err = io.Copy(fw, r); err != nil {
			return err
		}

	}
	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	_ = w.Close()
	req, err := http.NewRequest(reqData.GetMethod(), uri, &buffer)
	req.Header.Set("Content-Type", w.FormDataContentType())
	return client.doRequest(req, result)
}

func (client *Client) doRequest(req *http.Request, result IDataResponse) (err error) {
	req.Header.Set("x-ca-accesskeyid", client.AccessKey)
	req.Header.Set("x-ca-version", client.Version)
	req.Header.Set("user-agent", "product-ai SDK for golang, bitbucket.org/isapoetra/product-ai")
	httpClient := &http.Client{}
	resp, err := httpClient.Do(req)
	if err != nil {
		return err
	}
	defer func() {
		err := resp.Body.Close()
		if err != nil {
			log.Println(err)
		}
	}()

	body, err := ioutil.ReadAll(resp.Body)
	if string(body) == "" {
		log.Println("warning!! empty response")
		return nil
	}
	Dump("Response : " + string(body))
	if err == nil {
		err = json.Unmarshal(body, &result)
		if err != nil {
			return err
		}
	}

	if resp.StatusCode == http.StatusOK {
		return nil
	} else {
		if result.GetErrorCode() != 0 {
			return errors.New(strconv.Itoa(result.GetErrorCode()) + ":" + result.GetMessage())
		}
	}
	return err
}
func (client *Client) getJSONResponse(reqData IDataRequest, data interface{}, result IDataResponse) error {
	js, err := json.Marshal(data)
	if err != nil {
		return err
	}
	log.Printf("Params %s\n", string(js))
	uri := client.getUri(reqData)
	log.Println(reqData.GetMethod() + ":" + uri)
	req, err := http.NewRequest(reqData.GetMethod(), uri, bytes.NewBuffer(js))
	req.Header.Set("Content-Type", "application/json")

	return client.doRequest(req, result)
}
func DefaultClient(accessKey string, secret string) *Client {
	return &Client{
		AccessKey: accessKey,
		SecretKey: secret,
		Host:      "api.productai.cn",
		Scheme:    "https",
		Version:   "1",
		Verbose:   true,
	}
}
