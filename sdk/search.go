/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"strings"
)

type SearchRequest struct {
	ServiceId string `json:"service_id"`
}

type SearchData struct {
	Url   string             `json:"url,omitempty"`
	Count int                `json:"count,omitempty"`
	Tags  *SearchTagsBuilder `json:"tags"`
	File  string             `json:"file"`
	Loc   []float64          `json:"loc"`
}
type SearchResponseData struct {
	Metadata string `json:"metadata"`
	Url      string `json:"url"`
}
type ListSearchResponse struct {
	BaseDataResponse
	DetectTime     string   `json:"detect_time"`
	DetectionScore float64  `json:"detection_score"`
	DownloadTime   string   `json:"download_time"`
	ProcessTime    int      `json:"process_time"`
	RequestId      string   `json:"request_id"`
	Ds             string   `json:"ds"`
	IsErr          int      `json:"is_err"`
	Loc            []int    `json:"loc"`
	MostCommonTags []string `json:"most_common_tags"`

	SearchTime string               `json:"searchtime"`
	Time       string               `json:"time"`
	Type       []string             `json:"type"`
	Version    string               `json:"ver"`
	Results    []SearchResponseData `json:"results"`
}

func (response ListSearchResponse) GetErrorCode() int {
	return response.ErrorCode
}
func (response ListSearchResponse) GetMessage() string {
	return response.Message
}

func (search SearchRequest) GetEndPoint() string {
	return "search/" + search.ServiceId
}

func (search SearchRequest) GetResponseType() string {
	return ResponseTypeJson
}
func (search SearchRequest) GetMethod() string {
	return "POST"
}

func (client *Client) Search(serviceId string, params *SearchData) (*ListSearchResponse, error) {
	result := &ListSearchResponse{}
	sParams := map[string]io.Reader{}
	if params.Url != "" {
		sParams["url"] = bytes.NewBuffer([]byte(params.Url))
	}
	if params.Tags != nil {
		sParams["tags"] = bytes.NewBuffer([]byte(params.Tags.Build()))
	}
	if params.File != "" {
		f, err := os.Open(params.File)
		if err != nil {
			return nil, err
		}
		sParams["search"] = f
	}
	if len(params.Loc) > 0 {
		var loc []string
		for _, tmpLoc := range params.Loc {
			loc = append(loc, fmt.Sprintf("%f", tmpLoc))
		}
		sParams["loc"] = bytes.NewBuffer([]byte(strings.Join(loc, "-")))
	}

	err := client.postForm(SearchRequest{
		ServiceId: serviceId,
	}, sParams, result)
	return result, err
}
