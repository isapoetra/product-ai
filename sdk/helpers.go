/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import (
	"encoding/json"
	"log"
	"os"
)

func LoadConfig(fileName string, config interface{}) error {
	file, err := os.Open(fileName)
	if err == nil {
		decoder := json.NewDecoder(file)
		err = decoder.Decode(&config)
		if err != nil {
			panic(err)
		}
	}
	return err
}

func ToJson(a interface{}) string {
	res, err := json.Marshal(a)
	if err != nil {
		return ""
	}
	return string(res)
}
func Dump(a interface{}) {
	res, err := json.MarshalIndent(a, "", "\t")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(string(res))

}
