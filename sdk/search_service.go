/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import "time"

type ImageSearchData struct {
	Name     string `json:"name"`
	Scenario string `json:"scenario"` //https://developers.productai.com/en/reference/index#Scenarios
}
type SearchServiceResponse struct {
	BaseDataResponse
	CreatedAt       time.Time `json:"created_at"`
	CreatorId       int       `json:"creator_id"`
	Id              string    `json:"id"`
	ImageSetId      string    `json:"image_set_id"`
	IndexedRatio    int       `json:"indexed_ratio"`
	LastIndexedTime time.Time `json:"last_indexed_time,omitempty"`
	LastUpdatedAt   time.Time `json:"last_updated_at,omitempty"`
	NumberIndexed   int       `json:"n_indexed"`
	Name            string    `json:"name"`
	RequestId       string    `json:"request_id"`
	Scenario        string    `json:"scenario"`
	Status          string    `json:"status"`
	StatusDuration  int       `json:"status_duration"`
}

func (response *SearchServiceResponse) GetErrorCode() int {
	return response.ErrorCode
}
func (response *SearchServiceResponse) GetMessage() string {
	return response.Message
}

type ListSearchServiceResponse struct {
	RequestId string `json:"request_id"`
	Results   []SearchServiceResponse
}

func (response *ListSearchServiceResponse) GetErrorCode() int {
	return 0
}

func (response *ListSearchServiceResponse) GetMessage() string {
	return ""
}

func (client *Client) CreateSearchService(imageSetId string, params ImageSearchData) (*SearchServiceResponse, error) {
	result := &SearchServiceResponse{}
	err := client.GetResponse(CreateSearchRequest{
		ImageSetId: imageSetId,
	}, params, result)
	return result, err
}
func (client *Client) ModifySearchService(serviceId string, params ModifyServiceData) (*SearchServiceResponse, error) {
	result := &SearchServiceResponse{}
	err := client.GetResponse(ModifySearchRequest{
		ServiceId: serviceId,
	}, params, result)
	return result, err
}

func (client *Client) ListSearchService() (*ListSearchServiceResponse, error) {
	result := &ListSearchServiceResponse{}
	err := client.GetResponse(ListImageSearchRequest{}, nil, result)
	return result, err
}

func (client *Client) DeleteSearchService(serviceId string) (*SearchServiceResponse, error) {
	result := &SearchServiceResponse{}
	err := client.GetResponse(DeleteImageSearchRequest{
		ServiceId: serviceId,
	}, nil, result)
	return result, err
}
