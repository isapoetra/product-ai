/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

type ListImageSetRequest struct {
}

func (d ListImageSetRequest) GetEndPoint() string {
	return "get_image_sets/_0000014"
}
func (d ListImageSetRequest) GetResponseType() string {
	return ResponseTypeJson

}
func (d ListImageSetRequest) GetMethod() string {
	return "GET"
}

type ListImageSetResponse struct {
	BaseDataResponse
	Results []ImageSetResponse `json:"results"`
}

func (response *ListImageSetResponse) GetErrorCode() int {
	return response.ErrorCode
}
func (response *ListImageSetResponse) GetMessage() string {
	return response.Message
}
