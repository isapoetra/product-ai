/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import (
	"os"
	"testing"
)

var (
	client *Client
)

func init() {
	config := AIConfig{
		AccessKey: os.Getenv("PRODUCT_AI_ACCESS_KEY"),
	}
	client = DefaultClient(config.AccessKey, config.SecretKey)

}
func TestMain(m *testing.M) {
	// Do your stuff here
	os.Exit(m.Run())
}
