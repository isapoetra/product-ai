/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import "testing"

func TestSearchService(t *testing.T) {
	var result *SearchServiceResponse
	var err error
	var id string
	t.Run("CreateSearchService", func(t *testing.T) {
		result, err = client.CreateSearchService("d7tm32i7", ImageSearchData{
			Name:     "Search Service",
			Scenario: "fashion_5_7",
		})
		Dump(result)
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		id = result.Id
	})
	Dump(id)
	t.Run("ListSearchService", func(t *testing.T) {
		sResult, err := client.ListSearchService()
		Dump(sResult)
		if err != nil {
			t.Error(err)
			t.Fail()
			return
		} else {
			if id == "" {
				id = sResult.Results[0].Id
			}
		}
	})

	t.Run("ModifySearchService", func(t *testing.T) {
		//Modify
		result, err = client.ModifySearchService(id, ModifyServiceData{Name: "Fashion Search Service"})
		Dump(result)
		if err != nil {
			t.Error(err)
			t.Fail()
			return
		}
	})

	t.Run("DeleteSearchService", func(t *testing.T) {
		//Delete
		result, err = client.DeleteSearchService(id)
		Dump(result)
		if err != nil {
			t.Error(err)
			t.Fail()
			return
		}
	})

}
