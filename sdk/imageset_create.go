/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import "time"

type ImageSetParams struct {
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
}
type ImageSetResponse struct {
	BaseDataResponse
	CreatedAt        time.Time `json:"created_at"`
	CreatorId        int       `json:"creator_id"`
	Description      string    `json:"description"`
	Id               string    `json:"id"`
	ModifiedAt       time.Time `json:"modified_at"`
	NumberDownloaded int       `json:"n_downloaded"`
	NumberFailed     int       `json:"n_failed"`
	NumberItems      int       `json:"n_items"`
	Name             string    `json:"name"`
}

func (response *ImageSetResponse) GetErrorCode() int {
	return response.ErrorCode
}
func (response *ImageSetResponse) GetMessage() string {
	return response.Message
}

type ImageSetRequest struct {
	Endpoint string
}

func (d ImageSetRequest) GetEndPoint() string {
	return "image_sets/_0000014"
}
func (d ImageSetRequest) GetResponseType() string {
	return ResponseTypeJson

}
func (d ImageSetRequest) GetMethod() string {
	return "POST"
}
