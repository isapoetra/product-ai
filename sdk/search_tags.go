/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import (
	"reflect"
)

type ISearchTag interface {
	Add(interface{})
	Build() string
	GetTags() []interface{}
}

type SearchAndTag struct {
	tags []interface{}
}

func (andTag *SearchAndTag) Add(tag interface{}) {
	andTag.tags = append(andTag.tags, tag)
}
func (andTag *SearchAndTag) Build() string {
	if len(andTag.tags) == 0 {
		return ""
	}
	return BuildTag(andTag, "and")
}
func (andTag *SearchAndTag) GetTags() []interface{} {

	return andTag.tags
}

type SearchOrTag struct {
	tags []interface{}
}

func (orTag *SearchOrTag) Add(tag interface{}) {
	orTag.tags = append(orTag.tags, tag)
}
func (orTag *SearchOrTag) Build() string {
	if len(orTag.tags) == 0 {
		return ""
	}
	return BuildTag(orTag, "or")
}
func (orTag *SearchOrTag) GetTags() []interface{} {
	return orTag.tags
}

func BuildTag(searchTags ISearchTag, name string) string {
	result := "{ \"" + name + "\":[ "
	searchTagType := reflect.TypeOf((*ISearchTag)(nil)).Elem()
	for _, tag := range searchTags.GetTags() {
		tagType := reflect.TypeOf(tag)
		if tagType.Implements(searchTagType) {
			stags := tag.(ISearchTag)
			result += stags.Build()
		} else if tagType.String() == "string" {
			result += "\"" + tag.(string) + "\""
		}
		result += ","
	}
	result = result[:len(result)-1]
	result += "]}"
	return result
}

type SearchTagsBuilder struct {
	Tag ISearchTag `json:"tag"`
}

func (builder SearchTagsBuilder) Build() string {
	if builder.Tag == nil {
		return ""
	}
	return builder.Tag.Build()
}
