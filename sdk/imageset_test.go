/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import (
	"testing"
)

func TestImageSet(t *testing.T) {
	var id string
	t.Run("Create", func(t *testing.T) {
		params := &ImageSetParams{
			Name:        "NAME",
			Description: "Descr",
		}
		result, err := client.CreateImageSet(params)
		if err != nil {
			t.Error(err)
			t.Fail()
		} else {
			id = result.Id
		}
		Dump(result)
	})
	t.Run("TestDataInsertSingle", func(t *testing.T) {
		result, err := client.DataInsertSingle(id, DataInsertData{
			ImageUrl: "http://brandstore.vistaprint.in/render/undecorated/product/PVAG-0Q4K507W3K1Y/RS-K82Q06W655QY/jpeg?compression=95&width=700",
			Meta:     &DataInsertMeta{},
			Tags: []string{
				"Shirt",
				"brandstore",
			},
		})
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(result)
	})

	t.Run("TestDataDeleteMultiple", func(t *testing.T) {
		result, err := client.DataDeleteMultiple(id, "assets/demo.csv")
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(result)
	})
	t.Run("TestDataInsertOrUpdateMultiple", func(t *testing.T) {
		result, err := client.DataInsertOrUpdateMultiple(id, "assets/demo.csv")
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(result)
	})
	t.Run("ListImageSet", func(t *testing.T) {
		result, err := client.ListImageSet()
		if err != nil {
			t.Error(err)
			t.Fail()
		} else {
			if id == "" {
				id = result.Results[len(result.Results)-1].Id
			}
		}
		Dump(result)
	})
	t.Run("FetchImageSet", func(t *testing.T) {
		result, err := client.FetchImageSet(id)
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(result)
	})
	t.Run("ModifyImageSet", func(t *testing.T) {
		result, err := client.ModifyImageSet(id, ImageSetParams{Name: "Kodok"})
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(result)
	})

	t.Run("DeleteImageSet", func(t *testing.T) {
		result, err := client.DeleteImageSet(id)
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(result)
	})
}
