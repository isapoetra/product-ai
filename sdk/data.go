/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import (
	"io"
	"os"
	"strings"
)

type DataInsertData struct {
	ImageUrl string      `json:"image_url"`
	Meta     interface{} `json:"meta"`
	Tags     []string    `json:"tags"`
}
type DataInsertMeta struct {
}

func toDataInsertParams(data DataInsertData) map[string]string {
	return map[string]string{
		"image_url": data.ImageUrl,
		"meta":      ToJson(data.Meta),
		"tags":      strings.Join(data.Tags, "|"),
	}
}
func (client *Client) DataInsertOrUpdateMultiple(imageSetId string, file string) (*ImageSetResponse, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	resp := &ImageSetResponse{}
	err = client.postForm(DataInsertRequest{
		ImageSetId: imageSetId,
	}, map[string]io.Reader{
		"urls_to_add": f,
	}, resp)
	if err != nil {
		return nil, err
	}
	return resp, err
}
func (client *Client) DataDeleteMultiple(imageSetId string, file string) (*ImageSetResponse, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	resp := &ImageSetResponse{}
	err = client.postForm(DataInsertRequest{
		ImageSetId: imageSetId,
	}, map[string]io.Reader{
		"urls_to_delete": f,
	}, resp)
	if err != nil {
		return nil, err
	}
	return resp, err
}

func (client *Client) DataInsertSingle(imageSetId string, params DataInsertData) (*ImageSetResponse, error) {
	resp := &ImageSetResponse{}
	err := client.GetResponse(DataInsertRequest{
		ImageSetId: imageSetId,
	}, toDataInsertParams(params), resp)
	if err != nil {
		return nil, err
	}
	return resp, err
}
