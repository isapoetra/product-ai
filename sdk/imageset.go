/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

func (client *Client) CreateImageSet(params *ImageSetParams) (*ImageSetResponse, error) {
	resp := &ImageSetResponse{}
	err := client.GetResponse(ImageSetRequest{}, params, resp)
	if err != nil {
		return nil, err
	}
	return resp, err

}

func (client *Client) ListImageSet() (*ListImageSetResponse, error) {
	imageSet := ListImageSetRequest{}
	result := &ListImageSetResponse{}
	err := client.GetResponse(imageSet, nil, result)
	return result, err
}
func (client *Client) FetchImageSet(imageSetId string) (*ImageSetResponse, error) {
	result := &ImageSetResponse{}
	err := client.GetResponse(FetchImageSetRequest{
		Id: imageSetId,
	}, nil, result)
	return result, err
}
func (client *Client) ModifyImageSet(imageSetId string, params ImageSetParams) (*ImageSetResponse, error) {
	result := &ImageSetResponse{}
	err := client.GetResponse(ModifyImageSetRequest{
		Id: imageSetId,
	}, params, result)
	return result, err
}

func (client *Client) DeleteImageSet(imageSetId string) (*ImageSetResponse, error) {
	result := &ImageSetResponse{}
	err := client.GetResponse(DeleteImageSetRequest{
		Id: imageSetId,
	}, nil, result)
	return result, err
}
