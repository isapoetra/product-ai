/*
 * Product-ai golang sdk
 * Copyright (C) 2019 Cipta Graha Informatika Indonesia (CGII)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * Written by Iwan Sapoetra <isapoetra@gmail.com>
 *
 */

package sdk

import "testing"

func TestSearch(t *testing.T) {
	searchServiceId := "e9nugykv"
	t.Run("Search Image ByUrl", func(t *testing.T) {
		response, err := client.Search(searchServiceId, &SearchData{
			Url:   "https://ecs7.tokopedia.net/img/cache/700/product-1/2018/11/29/27895892/27895892_ff00de82-cb70-4169-b6e3-07fe19045410_762_1100.jpg",
			Count: 2,
		})
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(response)
	})
	t.Run("Search Image ByUrl and tags", func(t *testing.T) {
		andTag := &SearchAndTag{}
		andTag.Add("TMALL")
		andTag.Add("TAOBAO")
		orTag := &SearchOrTag{}
		orTag.Add("hi")
		andTag.Add(orTag)
		tags := SearchTagsBuilder{
			Tag: andTag,
		}

		response, err := client.Search(searchServiceId, &SearchData{
			Url:   "https://ecs7.tokopedia.net/img/cache/700/product-1/2018/11/29/27895892/27895892_ff00de82-cb70-4169-b6e3-07fe19045410_762_1100.jpg",
			Count: 2,
			Tags:  &tags,
		})
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(response)
	})
	t.Run("Search Image By File", func(t *testing.T) {
		response, err := client.Search(searchServiceId, &SearchData{
			File:  "./assets/shoes.png",
			Count: 2,
		})
		if err != nil {
			t.Error(err)
			t.Fail()
		}
		Dump(response)
	})
}
