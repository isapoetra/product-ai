this sdk for accessing [product-ai sdk] with golang

this sdk required  Go version 1.9 or greater.

## Usage ##
```go
import ai "bitbucket.org/isapoetra/product-ai/sdk"
```

## Quick start ##

```go
package main
import ai "bitbucket.org/isapoetra/product-ai/sdk"
func main() {
    config := ai.AIConfig{}
    err:= ai.LoadConfig("app.json",&config)
    if err != nil {
    	panic(err)
    }
    client := ai.DefaultClient(config.AccessKey, config.SecretKey)
	response, err := client.Search("serviceId", &ai.SearchData{
		Url:   "url image",
		Count: 2,
	})
	ai.Dump(response)
}
```